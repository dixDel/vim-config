"Disable Vi compatibility to benefit from all the Vim improvements.
"This must be first, because it changes other options as side effect.
set nocompatible

"required by Vundle
filetype off

" Force loading python3
if has('python3')
endif

" ===== Vundle =====
" Brief help
" :BundleList          - list configured bundles
" :BundleInstall(!)    - install(update) bundles
" :BundleSearch(!) foo - search(or refresh cache first) for foo
" :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
" see :h vundle for more details or wiki for FAQ

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()
"First installation of vundle: clone the git repository:
"$ git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
"let Vundle manage Vundle
Plugin 'gmarik/vundle'

" Other bundles grouped by origin:
" NOTE: comments after Bundle command are not allowed..
" Vundle: original repos on github
Plugin 'altercation/vim-colors-solarized'

Plugin 'wesleyche/SrcExpl'

Plugin 'tpope/vim-surround'

Plugin 'sjl/gundo.vim'

Plugin 'ctrlpvim/ctrlp.vim'

Plugin 'jwalton512/vim-blade'

Plugin 'godlygeek/tabular'

" Markdown Vim Mode
" Must be installed after Tabular
Plugin 'plasticboy/vim-markdown'

Plugin 'scrooloose/syntastic'

Plugin 'msanders/snipmate.vim'

" Fast HTML writing (zencoding)
Plugin 'mattn/emmet-vim'

" File system explorer, easier than netrw
Plugin 'scrooloose/nerdtree'

" Toggle comments, supporting many file types: hit <Leader>c<space>
Plugin 'scrooloose/nerdcommenter'

" Customize status bar
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" auto completion
Plugin 'ludovicchabant/vim-gutentags'

" PHP Refactoring Toolbox for VIM
Plugin 'adoy/vim-php-refactoring-toolbox'

Plugin 'arnaud-lb/vim-php-namespace'

" Automatically add getter/setters for PHP
" :InsertBothGetterSetter, :InsertGetterOnly, :InsertSetterOnly
" Default binding are <LocalLeader>b, <LocalLeader>g, <LocalLeader>s
" By default, <LocalLeader> is the backslash \ (on my French keyboard is AltGr+8)
" IMPORTANT: properties must be written using the following format: private $name = '';
" Do not write comma separated properties.
" Commented because I use php_getset_dixDel.vim (no github)
"Plugin 'EvanDotPro/php_getset.vim'
Plugin 'file:///home/didier/Computer science & Programming/Vim/Plugins/php_getset_dixDel.vim'

" Smart indenting by filetype, better than smartindent
" Also required by Vundle
filetype plugin indent on
" ===== End Vundle =====

" ===== Vim configuration =====
"Auto indentation for code blocks
"Don't use smartindent because it would do "stupid things" with comments
set autoindent
" Copy the previous indentation on autoindenting
set copyindent

" Use UTF-8 encoding
set encoding=utf-8

"Make bash aliases available
"Sources
"   jeffrey@jeffrey-way.com - http://net.tutsplus.com
"   https://github.com/bukzor/buck_dotfiles/blob/master/.vimrc
set shell=/bin/bash
set shellcmdflag=-c

"When a bracket is inserted, briefly jump to the matching one
set showmatch

" Prevent security exploits with modelines in files.
set modelines=0
" Create <filename>.un~ whenever a file is edited, containing undo information to allow undoing actions even after a file is closed then reopened
set undofile
set undodir=/home/didier/.vim/undo
set undolevels=1000
set undoreload=10000

" Use a tags file for autocompletion.
" Updated version of ctags (2013-10-14) patched for php namespaces, traits, etc.: https://github.com/shawncplus/phpcomplete.vim/wiki/Patched-ctags
" Go to the top-level directory of a project containing PHP files and run:
" $ ctags -R --fields=+aimS --languages=php,javascript,html,sql
" It creates a file named "tags" in the current directory.
" To tell Vim to use it:
" It means "look for a tags file in the directory of the current file, then upward until / and in the working directory, then upward until /".
" In other words, no matter where you are in your project, Vim will pick up the right tags for the project.
" Note: the magic part of that line is the ; of "upward search". You can give Vim an upper limit like this: ;$HOME
" http://stackoverflow.com/questions/19330843/how-do-i-automatically-load-a-tag-file-from-a-directory-when-changing-to-that-di
set tags=./tags;,tags;

" Better line wrapping
set wrap
set textwidth=0
" However, in Git commit messages, let’s make it 72 characters
if has("autocmd")
    autocmd FileType gitcommit set textwidth=72
    autocmd FileType gitcommit set colorcolumn=73
endif

" Activate backup
set backup
" Write backup file before saving
set writebackup
set backupdir=~/.vim/backup,~/.tmp,~/tmp,/var/tmp,/tmp
set directory=~/.vim/tmp,~/.tmp,~/tmp,/var/tmp,/tmp

set wrapmargin=0
set formatoptions=qrn1

" Needed for listchars.
set list
" This line will make Vim set out tab characters, trailing whitespaces and invisible spaces visually,
" and additionally use the # sign at the end of lines to mark lines that extend off-screen.
set listchars=tab:▸\ ,eol:¬,trail:_,extends:#,nbsp:_
"Enable folding
set foldenable
" Open most fold by default (0:all closed > 99:all opened)
set foldlevelstart=10
" Limit fold nesting
set foldnestmax=10
" Fold based on indentation (default is manual :help foldmethod
set foldmethod=indent
"Files to ignore
set wildignore+=*.bak,*.tmp,*.swp
"Make vim completion menu work like in an IDE
set completeopt=longest,menuone
"Add the dash for built in autocomplete
set iskeyword+=-
" Redraw screen only when really useful (speedier)
set lazyredraw
" Auto increment: add alpha to default options
set nrformats=octal,hex,alpha

" Change the terminal's title.
set title
"General usability {
    "turn off the "ding!"
    set visualbell
    set noerrorbells
" Switch to paste mode before pasting a text, so Vim disable all kinds of "smartness" like smart indents.
" When in Insert mode, type F3.
set pastetoggle=<F3>
" Tip: in Insert mode, type <C-r>+ to paste right from the OS paste board.
" Only works when running Vim locally.

"Session settings
set sessionoptions=buffers,curdir,help,options,tabpages,winsize

set autowrite

"Set crypto method
"Save file the first time with :X
"@source
"https://www.generation-linux.fr/index.php?post/2017/08/06/Chiffrer-simplement-un-fichier-texte-avec-Vim
setlocal cm=blowfish2
" ===== End vim configuration =====

" ===== Display settings =====
if has("gui_running")
    set guifont=Inconsolata\ Bold\ 14
    " Clean up the GUI in Gvim
    set guioptions-=T
    set guioptions-=m
    set guioptions+=LlRrb " bug?
    set guioptions-=LlRrb
    " To remain compatible with older versions of Vim that do not have the autocmd functions,
    " always wrap those functions inside a block like this:
    if has("autocmd")
        " Automatically resize splits
        " when resizing gui Vim window
        autocmd VimResized * wincmd =
    endif
    " Hide mouse when typing
    set mousehide
    "@source http://vim.wikia.com/wiki/Show_tab_number_in_your_tab_line
    set showtabline=2
    " always show tabs in gvim, but not vim
    set guitablabel=%{GuiTabLabel()}
endif

" Shows line, column and relative position of the cursor
set ruler

" Display line number relative to the cursor line
set relativenumber

" Display current mode
set showmode

" Display command entered in bottom right
set showcmd

" Remember the last 1000 commands on the command-line
set history=1000

" Hide buffers instead of unloading them
" If we unload a buffer it means it's local history/undo stack
" will be gone if we switch buffers
set hidden

" Display a colored column at <number> characters (help seeing I'm writing a too long line of code)
" 80 is PHP PSR-2 recommended length limit
set colorcolumn=81

" Colors for wide lines.
" Only used with js and php files (see autocmd block below) because
" it seems to lag sometimes
highlight ColorColumnSuggestedMax ctermbg=DarkYellow
highlight ColorColumnMax ctermbg=DarkRed
" ===== End display settings =====

" ===== Tab settings =====
" One tab is 4 spaces
set tabstop=4
" Number of spaces for (auto)indent
set shiftwidth=4
" Use multiple of shiftwidth when indenting with '<' and '>'
set shiftround
" Uses spaces instead of tabs
set expandtab
set softtabstop=4
" Backspace to remove space-indents
" Insert tabs on the start of a line according to shiftwidth, not tabstop.
set smarttab

" ===== Syntax =====
" Turn on syntax highlighting
syntax on
colorscheme solarized
set background=dark

" A menu for resolving ambiguous tab-completion
set wildmenu
" List all matches and complete till longest common string
set wildmode=full

" Always show the status line
set laststatus=2

" Keep lines visible when scrolling the page
set scrolloff=8

" Highlight the cursor line
set cursorline

" Allow backspace to delete end of line, indent and start of line characters
set backspace=indent,eol,start

" Set a different map Leader (default: \)
let mapleader="\<Space>"

" ===== Search/yank settings =====
" Ignore case
set ignorecase
" ...unless the search uses uppercase letters
set smartcase
" Set incremental searching (search as you type)
set incsearch
" Highlight the search
set hlsearch

" ===== Plugins settings =====
" === Tag List === (this plugin has been installed manually)
nmap <F6> :TlistToggle<CR>
let Tlist_Auto_Open = 0
let Tlist_Exit_OnlyWindow = 1
let Tlist_Sort_Type = "name"

" === Ctrlp ===
nnoremap <Leader>p :CtrlP<CR>
nnoremap <Leader>b :CtrlPBufTagAll<CR>
" Run Ctrlp using current word
nmap <Leader>pp <C-P><C-\>w
" Same behavior in Visual mode
" Ctrl-U delete the range displayed by default
" A space is needed between <C-\> and v or it is not executed.
vmap <Leader>pp :<C-U>CtrlP<CR><C-\> v
" Jump to definition, using Gutentags
map <silent> <leader>jd :CtrlPTag<cr><C-\>w
" persist cache
" let g:ctrlp_cache_dir = $HOME . '/.cache/ctrlp'
let g:ctrlp_show_hidden = 1
let g:ctrlp_use_caching = 0
" disable ctrlp's feature where it tries to intelligently work out the current
" working directory to search within
let g:ctrlp_working_path_mode = 'ar'
" don't let ctrlp take over the screen!
let g:ctrlp_max_height=30
" ignore those directories
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.(git|hg|svn)|Cache|node_modules|open-iconic|Tests\/coverage|Tests\/pdepend|vendor)$',
  \ 'file': '\v\.(exe|so|dll)$',
  \ 'link': 'some_bad_symbolic_links',
  \ }

" === Source Explorer ===
nmap <F7> :SrcExplToggle<CR>
" // In order to avoid conflicts, the Source Explorer should know what plugins
" // except itself are using buffers. And you need add their buffer names into
" // below listaccording to the command ":buffers!"
let g:SrcExpl_pluginList = [
    \ "__Tag_List__",
    \ "_NERD_tree_",
    \ "ControlP"
\ ]
" // Do not let the Source Explorer update the tags file when opening
let g:SrcExpl_isUpdateTags = 0

" === Gundo ===
nnoremap <F5> :GundoToggle<CR>

" === vim-blade ===
" Define some single Blade directives. This variable is used for highlighting only.
let g:blade_custom_directives = ['datetime', 'javascript']
" Define pairs of Blade directives. This variable is used for highlighting and indentation.
let g:blade_custom_directives_pairs = {
      \   'markdown': 'endmarkdown',
      \   'cache': 'endcache',
      \ }

" === Tabularize ===
" Use :Tabularize <tab> to display several text options (e.g.: assignment)
" :Tab /<some character(s)> e.g. :Tab /=>
nnoremap <Leader>t= :Tab /=<CR>
vnoremap <Leader>t= :Tab /=<CR>
nnoremap <Leader>t: :Tab /:<CR>
vnoremap <Leader>t: :Tab /:<CR>
" align ->
nnoremap <Leader>t» :Tab /\v\-\>.*<CR>
vnoremap <Leader>t» :Tab /\v\-\>.*<CR>
" align =>
nnoremap <Leader>t3 :Tab /\v\=\><CR>
vnoremap <Leader>t3 :Tab /\v\=\><CR>
" align $variable
nnoremap <Leader>t$ :Tabularize /\v\$[a-z]+<CR>
vnoremap <Leader>t$ :Tabularize /\v\$[a-z]+<CR>

" === Markdown ===
" Disable folding.
let g:vim_markdown_folding_disabled=1
" Set initial fold level (default 0 means all folds are closed).
" This option is ignored if folding is disabled.
let g:vim_markdown_initial_foldlevel=1

" === Asynchronous Lint Engine ===
Plugin 'w0rp/ale'
let g:ale_linters = {
\   'php': ['php'],
\}
let g:ale_lint_on_save = 1
let g:ale_lint_on_text_changed = 0

" === Syntastic ===
" Syntax checking plugin
" On by default, turn it off for html
let g:syntastic_mode_map = { 'mode': 'active', 'active_filetypes': [],'passive_filetypes': ['html'] }
" Run phpcs by default, which take too much time, so ask it to only exec php
let g:syntastic_php_checkers=['php']
let g:syntastic_filetype_map = { 'utphp': 'php', 'itphp': 'php' }
" $ sudo npm install -g jslint
let g:syntastic_javascript_checkers=['jshint']
let g:syntastic_javascript_jshint_args=""
" Better :sign interface symbols
let g:syntastic_error_symbol = '✗'
let g:syntastic_warning_symbol = '!'
let g:syntastic_enable_signs=1
let g:syntastic_echo_current_error=1
let g:syntastic_aggregate_errors = 1
" By default the location list is changed only when you run the :Errors command,
" in order to minimise conflicts with other plugins.
" This update the location list automatically (disable if there is conflicts
" with other plugins)
let g:syntastic_always_populate_loc_list=1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" === emmet-vim ===
" ex: div#page>div.logo+ul#navigation>li*5>a_ where "_" is the cursor position.
" Then type <C-y>, (CTRL Y comma) to generate the HTML.
" Change the default trigger (the trailing comma will still need to be entered)
let g:emmet_html5 = 0

" === gutentags ===
" Where to store tag files
" let g:gutentags_cache_dir = '~/.vim/gutentags'
let g:gutentags_ctags_exclude = ['*.css', '*.html', '*.js', '*.json', '*.xml',
    \ '*.phar', '*.ini', '*.rst', '*.md',
    \ '*vendor/*/test*', '*vendor/*/Test*',
    \ '*vendor/*/fixture*', '*vendor/*/Fixture*',
    \ '*var/cache*', '*var/log*']
"let g:airline#extensions#gutentags#enabled = 1

" === hardmode ===
if has("autocmd")
    autocmd VimEnter,BufNewFile,BufReadPost * silent! call HardMode()
endif

" === vim-php-namespace ===
" insert 'use' statements
function! IPhpInsertUse()
    call PhpInsertUse()
    call feedkeys('a',  'n')
endfunction
if has("autocmd")
    autocmd FileType php inoremap <Leader>pnu <Esc>:call IPhpInsertUse()<CR>
    autocmd FileType php noremap <Leader>pnu :call PhpInsertUse()<CR>
endif
" expand classes to get their fully qualified names
function! IPhpExpandClass()
    call PhpExpandClass()
    call feedkeys('a', 'n')
endfunction
if has("autocmd")
    autocmd FileType php inoremap <Leader>pne <Esc>:call IPhpExpandClass()<CR>
    autocmd FileType php noremap <Leader>pne :call PhpExpandClass()<CR>
endif
" automatically sort namespaces after inserting
let g:php_namespace_sort_after_insert=1

" ===== End plugin settings =====

" ===== Automatic =====
" Set file types for some of the PHP related extensions
if has("autocmd")
    autocmd BufNewFile,BufRead *.inc set ft=php
    autocmd BufNewFile,BufRead *.phtml set ft=phtml
    autocmd BufNewFile,BufRead *.tpl set ft=phtml

    " XML
    autocmd BufNewFile,BufRead *.xsd set ft=xml
    let g:xml_syntax_folding=1
    au FileType xml setlocal foldmethod=syntax

    au BufRead,BufNewFile *.twig set ft=html
endif
" ===== End automatic =====

" ===== Custom functions =====
" Highlight matches when jumping between them
function! HLNext (blinktime)
    highlight WhiteOnRed ctermfg=white ctermbg=red
    let [bufnum, lnum, col, off] = getpos('.')
    let matchlen = strlen(matchstr(strpart(getline('.'),col-1),@/))
    let target_pat = '\c\%#'.@/
    let ring = matchadd('WhiteOnRed', target_pat, 101)
    redraw
    exec 'sleep ' . float2nr(a:blinktime * 1000) . 'm'
    call matchdelete(ring)
    redraw
endfunction

function! ToggleNumber()
    if (&relativenumber == 1)
        set norelativenumber
        set number
    else
        set relativenumber
    endif
endfunction

function! NoNumber()
    set norelativenumber
    set nonumber
endfunction

" strips trailing whitespace at the end of files
" replace every tab with 4 spaces
" preserve cursor position
function! <SID>StripTrailingWhitespaces()
    " save last search & cursor position
    let _s=@/
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    %s/\t/    /ge
    let @/=_s
    call cursor(l, c)
endfunction

" Move current tab into the specified direction.
" @source http://stackoverflow.com/questions/7961581/is-there-a-vim-command-to-relocate-a-tab
" @param direction -1 for left, 1 for right.
function! TabMove(direction)
    " get number of tab pages.
    let ntp=tabpagenr("$")
    " move tab, if necessary.
    if ntp > 1
        " get number of current tab page.
        let ctpn=tabpagenr()
        " move left.
        if a:direction < 0
            let index=((ctpn-1+ntp-1)%ntp)
        else
            let index=(ctpn%ntp)
        endif
        " move tab page.
        execute "tabmove ".index
    endif
endfunction

" set up tab labels with tab number, buffer name, number of windows
function! GuiTabLabel()
    let label = ''
    let bufnrlist = tabpagebuflist(v:lnum)
    " Add '+' if one of the buffers in the tab page is modified
    for bufnr in bufnrlist
        if getbufvar(bufnr, "&modified")
            let label = '+'
            break
        endif
    endfor
    " Append the tab number
    let label .= v:lnum.': '
    " Append the buffer name
    let name = bufname(bufnrlist[tabpagewinnr(v:lnum) - 1])
    if name == ''
        " give a name to no-name documents
        if &buftype=='quickfix'
            let name = '[Quickfix List]'
        else
            let name = '[No Name]'
        endif
    else
        " get only the file name
        let name = fnamemodify(name,":t")
    endif
    let label .= name
    " Append the number of windows in the tab page
    let wincount = tabpagewinnr(v:lnum, '$')
    return label . '  [' . wincount . ']'
endfunction

" Move current buffer to a new gVim instance
" https://stackoverflow.com/questions/19553342/gvim-move-tab-to-new-window
function! MoveToNewGvim()
    :execute 'bdelete | !gvim' shellescape(expand('%:p'), 1)
endfunction

" ===== End custom functions =====

" ===== Remappings =====
" Make CTRL-U and CTRL-W in insert mode use CTRL-G U to start a new change.
" This make the changes recoverable with undo in normal mode.
" @source http://vim.wikia.com/wiki/Recover_from_accidental_Ctrl-U
inoremap <c-u> <c-g>u<c-u>
inoremap <c-w> <c-g>u<c-w>
" Save file
nnoremap <Leader>w :w<CR>
" Disable arrow keys to force myself to stay on the home row
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
inoremap <Up> <NOP>
inoremap <Down> <NOP>
inoremap <Left> <NOP>
inoremap <Right> <NOP>
" Make j and k moving by screen line instead of file line
nnoremap j gj
nnoremap k gk
nnoremap $ g$
nnoremap 0 g0
nnoremap gj j
nnoremap gk k
nnoremap g$ $
nnoremap g0 0
" highlight last inserted text
nnoremap gV `[v`]
" Remap F1 to act like ESC instead of displaying help
inoremap <F1> <ESC>
nnoremap <F1> <ESC>
vnoremap <F1> <ESC>
" Remap the backtick to single quote and vice versa.
" A Belgian keyboard send a accented character when typing a backtide ( ` ), so
" the input is waiting for you to type a character following.
" To type a ` alone, you need to type it twice.
" In Vim, backtick takes you to an exact mark location while single quote
" ( ' ) simply takes you to the beginning of the marked line.
nnoremap ' `
nnoremap ` '
" I often mistakingly save a file named <
nnoremap :w< :w
nnoremap :< :w
nmap <Leader>sw dawwP
vmap <Leader>sw dawwP
nmap <Leader>sW dawbP
vmap <Leader>sW dawbP

"Turn off Vim's default regex characters and make search use normal regexes (Perl like)
nnoremap / /\v
vnoremap / /\v
" substitution too
nnoremap :s/ :s/\v
vnoremap :s/ :s/\v
nnoremap :%s/ :%s/\v
vnoremap :%s/ :%s/\v
" <Ctrl-l> redraws the screen and removes any search Highlighting.
" Redrawing the screen solve the display bug happening after the rsync autocmd !
nnoremap <silent> <C-l> :nohlsearch<CR><C-l>
" Force syntax highlight because of unknown bug make it disappearing.
nnoremap <silent> <C-A-l> :syntax enable<CR><C-l>
" Rewire n and N to do the highlighting
nnoremap <silent> n n:call HLNext(0.4)<cr>
nnoremap <silent> N N:call HLNext(0.4)<cr>

" Open the word's definition by typing the following shortcuts when the cursor is over it.
" By default, Ctrl-] jumps to the definition and Ctrl-t jumps back to the
" call.
" Ctrl-] open in a new split
"nnoremap <silent><C-]> <C-w><C-]>
" Ctrl-Alt-] open in a new tab
nnoremap <silent><C-A-]> <C-w><C-]><C-w>T
nnoremap :tag :tab tag
"allow the . to execute once for each line of a visual selection
vnoremap . :normal .<CR>

noremap <silent> <F10> :NERDTreeToggle<CR>

map <Leader>n :call ToggleNumber()<CR>
map <Leader>nn :call NoNumber()<CR>

map <F8> :call TabMove(-1)<CR>
map <F9> :call TabMove(1)<CR>

"invert function parameter order
"http://stackoverflow.com/questions/4586329/vim-invert-functions-parameter-order
map <F2> "qdt,dwt)p"qp

" Use Q for formatting the current paragraph (or selection)
vmap Q gq
nmap Q gqap

" When I forget to sudo before editing a file that requires root privileges,
" just type w!! to save changes.
cmap w!! w !sudo tee % > /dev/null

nnoremap <silent> <Leader>mtng :call MoveToNewGvim()<CR>

" Shortcuts for opening files located in the same directory as the current
" file
" @source http://vimcasts.org/episodes/the-edit-command/
cnoremap %% <C-R>=fnameescape(expand('%:h')).'/'<CR>
map <leader>ew :e %%
map <leader>es :sp %%
map <leader>ev :vsp %%
map <leader>et :tabe %%

" Text bubbling (visually move blocks of text up and down)
" @source http://vimcasts.org/episodes/bubbling-text/
" Single line
" Edge cases:
"   - Bubbling up at first line of document will erase the line
"   - Bubbling up at last line of document will jump twice because of cursor
"   staying at the same line
nnoremap <C-j> ddp
nnoremap <C-k> ddkP
" Multiple lines (edited original example because `] put the cursor at the start
" of the next line (maybe cause by some settings here?)
" Edge cases:
"   - Bubbling up at first line and down at last line of document will erase
"   the block
"   - Bubbling up at last line of document will mess with line above the
"   selected block
vnoremap <C-j> xjP`[V`]ge
vnoremap <C-k> xkP`[V`]ge

" Visually select the text that was last edited/pasted
" @source http://vimcasts.org/episodes/bubbling-text/
nmap gV `[v`]

" Insert empty line without entering insert mode nor moving the cursor.
" https://stackoverflow.com/questions/3170348/insert-empty-lines-without-entering-insert-mode
map <Leader>O :<C-U>call append(line(".") -1, repeat([''], v:count1))<CR>
map <Leader>o :<C-U>call append(line("."), repeat([''], v:count1))<CR>

" Show what the current buffer is
map <Leader>y :CtrlPBuffer<CR>

" Remap commands history window because it pop ups too often when I just want to quit!
nnoremap q: :q
nnoremap qq: q:

" Execute populatePlaylist using the current line as argument.
"   - move cursor to start of line
"   - yank the line into the y register
"   - execute the substitute command directly on the register y and update it
"   to escape !
"   - execute populatePlaylist script passing a playlist name and the content of
"   y register as arguments
"   - ctrl l is the shortcut to refresh vim screen (avoid blank screen after each
" execution)
map <Leader>ate 0"yy$:let @y = substitute(@y, '!', '\\!', 'g')<CR>:silent !~/bin/populatePlaylist.sh energy "<C-r>y"<CR><C-l>
map <Leader>atc 0"yy$:let @y = substitute(@y, '!', '\\!', 'g')<CR>:silent !~/bin/populatePlaylist.sh calm "<C-r>y"<CR><C-l>

"Open splits
nmap vs :rightbelow vsplit<cr>
nmap sp :rightbelow split<cr>
" ===== End remappings =====

" ===== Custom hooks =====
if has("autocmd")
    augroup myvimrchooks
        au!
        " Enable spell checking
        " To move forward/backward through the spelling errors, press ]s or [s .
        " When the cursor is on a misspelled word, press z= to bring up a list of suggested corrections.
        autocmd BufRead,BufNewFile * setlocal spell
        " Enable autocompletion: press <C-N> or <C-P> to choose a completion
        set complete+=kspell
        autocmd BufWritePre *.php,*.js,*.html,*.*css,*.xml,*.sh,*.json,*.sql :call <SID>StripTrailingWhitespaces()
        " Source the vimrc file after saving it. This way, no need to reload Vim to see the changes
        "original example:
        "au BufWritePost .vimrc,_vimrc,vimrc,.gvimrc,_gvimrc,gvimrc so $MYVIMRC | if has('gui_running') | so $MYGVIMRC | endif
        autocmd BufWritePost .vimrc source $MYVIMRC
        autocmd BufWritePost *.snippets call ReloadAllSnippets()
        autocmd BufEnter * set listchars=tab:▸\ ,eol:¬,trail:_,extends:#,nbsp:_
        autocmd BufEnter *.md set listchars=tab:▸\
        " Color the 81st column only in wide lines (reaching that column)
        autocmd BufEnter *.js,*.php call matchadd('ColorColumnSuggestedMax', '\%81v', 100)
        " Color the 121st column only in wide lines (reaching that column)
        autocmd BufEnter *.js,*.php call matchadd('ColorColumnMax', '\%121v', 100)
        " Open epub files
        autocmd BufReadCmd *.epub call zip#Browse(expand("<amatch>"))
    augroup END
endif
" ===== End custom hooks =====
